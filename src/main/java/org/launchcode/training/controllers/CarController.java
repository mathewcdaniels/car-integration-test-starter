package org.launchcode.training.controllers;

import org.launchcode.training.models.Car;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * Created by LaunchCode
 */
@Controller
@RequestMapping("car")
public class CarController {

    @Autowired
    private org.launchcode.training.data.CarRepository carRepository;

    @RequestMapping(value ="/seedData")
    public String seedDatabase() {
        // I used this to insert records into the H2 file database
        // 1) I changed spring.jpa.hibernate.ddl-auto=create in application.properties
        // 2) created car objects and wrote code to persist them
        // 3) ran bootRun task
        // 4) when to /car/seedData in browser
        // 5) see cars at /car/ in browser
        // 6) change db setting back to update: spring.jpa.hibernate.ddl-auto=update
        Car prius = new Car("Toyota", "Prius", 10, 50);
        Car jeep = new Car("Jeep", "Grand Cherokee ", 20, 25);
        Car jetta = new Car("Volkswagen", "Jetta", 12, 35);
        carRepository.save(prius);
        carRepository.save(jeep);
        carRepository.save(jetta);
        return "car/seeded";
    }

    @RequestMapping
    public String index(Model model){
        List<Car> cars = carRepository.findAll();
        model.addAttribute("cars", cars);
        return "car/index";
    }

}
